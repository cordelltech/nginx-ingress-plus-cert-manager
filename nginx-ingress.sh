#!/bin/bash

ip=$1
namespace=${2:-default}
name=${3:-nginx-ingress}
replicas=${4:-2}

if [ -z $ip ]; then
  echo "IP is required as first argument"
  exit 1
fi

helm install stable/nginx-ingress --namespace $namespace --name $name \
--set controller.replicaCount=$replicas \
--set controller.ingressClass=nginx \
--set controller.scope.enabled=true \
--set controller.service.loadBalancerIP=$ip \
--set controller.scope.namespace=$namespace
