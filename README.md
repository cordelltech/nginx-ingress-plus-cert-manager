# Introduction 
The bash scripts included will install cert-manager and nginx ingress in k8s. They can be ran in any order.

# Prerequisites
- k8s cluster
- rbac enabled
- helm and tiller installed with proper role bindings
- nginx ingress (can use other types but this readme follows as using nginx ingress)

# Install
- run ``./install.sh``
- install the issuers into each namespace that will use cert manager with ``kubectl apply -f issuer.yaml``
- add the following annotations to each ingress yaml that will use cert manager
  ```
  kubernetes.io/tls-acme: "true"

  # issuer should match names in issuer.yaml ('letsencrypt-prod' or 'letsencrypt-stageing')
  certmanager.k8s.io/issuer: {{issuer}} 

  # challenge can be http01 or dns01 (may be other options)
  certmanager.k8s.io/acme-challenge-type: {{challenge}} 
  ```

# References
- https://akomljen.com/get-automatic-https-with-lets-encrypt-and-kubernetes-ingress/
- https://github.com/jetstack/cert-manager/blob/master/docs/tutorials/acme/quick-start/index.rst